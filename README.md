# Gitlab CICD Observability for pipelines

## Overview
This repo acts as an example of how you can instrument your CICD pipeline in gitlab. It is using honeycomb as the backend
for the data and using honeycombs [buildevents](https://github.com/honeycombio/buildevents) utility to send the data.

## buildevents
The buildevents util is a mostly CICD universal trace exporter for honeycomb. It supports most OS types of linux, windows
and mac. The main actions for the util are

### build
The `build` action is what creates the root span for all the other spans. However, is should be the last thing to be run. 
Since it needs to time the entire pipeline. The `build` action will calculate the span end time as the time when this action
is run. Which is why it needs to be the last thing to run in the pipeline. To ensure this we should put it in a `.post` 
stage which gitlab will assure runs last.

The action takes 3 parameters:
* **traceId** - This should be something that's unique on every pipeline. A good candidate here is the $CI_PIPELINE_ID as it will be unique on every pipeline but common to every job in the pipeline.
* **StartTime** - This is the time when the pipeline started. The easiest way to do this is to have a setup job that runs first and sets the start time into an artifact that can be read by the end job.
* **outcome** - This is a string but normally good values are `success` or `failed`

The `build` action will use the current time when its run as the end time for calculating the span duration.

**Example**
```shell
buildevents build $CI_PIPELINE_ID $BUILD_START success
```

### step
The `step` action has its roots from other CI systems like GitHub where a step is a collection of commands to be performed.
In gitlab these are essentially jobs. So anytime we want to record or instrument a job we should use the `step` action.

The action takes 4 parameters:
* **traceId** - This needs to match with the trace ID used in the build action and will be what links this span to the same trace as the build root span
* **stepId** - This should be a unique ID for this step (job). A good candidate here is the $CI_JOB_ID as it will be unique on every job
* **startTime** - This should be the time the job started as epoch. A good candidate is to convert the job start time gitlab gives us `STEP_START=$(date -d "$CI_JOB_STARTED_AT" +%s)`
* **name** - The name that will be used in the display of the trace. Generally I would just use $CI_JOB_NAME for this.

Like the build function this should be run as the last thing on a job since it will use the current time as the end date for the span

**Example**
```shell
buildevents step $CI_PIPELINE_ID $CI_JOB_ID $STEP_START $CI_JOB_NAME
```

### cmd
The `cmd` action acts as wrapper on any command that might be run within a job where you want to record the individual 
commands in a job. You may want to instrument all your commands or maybe just a few. The `cmd` action will exit with the 
result code of your command transparently. 

The action takes 3 parameters:
* **traceId** - This needs to match with the trace ID used in the build action and will be what links this span to the same trace as the build root span
* **stepId** - This needs to match the stepId used in the job stage. It will set the jobs spanId as the parent span for this span which allows us to get the hierarchical view
* **name** - This is the name that will be displayed in the trace for this command.

In addition to the above parameters after these you need a ` -- ` then the actual command you want to be run which `cmd` will rap.

**Example**
```shell
buildevents cmd $CI_PIPELINE_ID $CI_JOB_ID foo -- sleep $((RANDOM % 11))
```

## Auto-magically trace jobs
In order to minimise pipeline boilerplate code we can code up an intrumentation-pipeline template that other pipelines can
just include to allow them to be instrumented. 

### Automate the build action
To automate the build action we can take advantage of gitlabs `.pre` and `.post` stage that are special stages that are assured
to run before or after any other stages. This allows us to download the buildevents util and store it as an artifact so its
available in all jobs in the pipeline and set the build start time as part of the `.pre` stage. We can then use the `.post`
stage to run the `build` action to crate the root span with the full pipeline runtime.

### Automate the step action
In order to instrument every job we can create `before_script` and `after_script` templates and set them as the defaults for 
the pipeline. This means when any job runs our instrumentation scripts would run before and after the job ensuring that
we trace the job its self.

#### Jobs with their own before or after script
If the job defines its own `before_script` or `after_script` then it will need to also refrence our scripts, In the `before_script`
this should be the first item, in the `after_script` it shold be the last item.

**Example**
```yaml
another_job:
  before_script:
    - !reference [.instrument_job_before_script]
    - sleep 3
  script:
    - buildevents cmd $CI_PIPELINE_ID ${CI_JOB_ID} baz -- ls non_exist_file
  stage: build
```
### Automate the cmd action
There is no way to automate this and pipelines would just need to wrap the commands they want to instrument.

## Enriching traces
The buildevents util can also enrich a span with key value pairs from a file. It will use the `BUILDEVENT_FILE` variable
to point to a file. Any key value pairs in there will be added to the span. The way I have designed this is to create 
this file as part of the buildevent artifact in the prestage and populate it with values that should be in every span.

Examples would be things like CI_COMMIT_SHA, CI_PROJECT_NAMESPACE etc. These are useful bits of info to drive queries on 
the data.

**Example**
```yaml
# Set Key values that should be in all traces for this pipeline
- echo "CI_PROJECT_NAMESPACE=${CI_PROJECT_NAMESPACE}" >> ${BUILDEVENT_FILE}
- echo "CI_PROJECT_NAMESPACE_ID=${CI_PROJECT_NAMESPACE_ID}" >> ${BUILDEVENT_FILE}
- echo "CI_COMMIT_SHA=${CI_COMMIT_SHA}" >> ${BUILDEVENT_FILE}
```

These will be put in the file which is then uploaded as an artifact. Each stage will download the artifact and can then 
also enrich it with its own data that might apply only to that job. Any data a job adds to this file will not be persisted
to any other job.

```yaml
- echo "CI_JOB_STAGE=${CI_JOB_STAGE}" >> ${BUILDEVENT_FILE}
```

This can enrich the span and make slicing and dicing data easier to find specific parts.

![enriched-span-fields](./images/enriched-span-fields.png)

## Traces
Now that we have our pipeline insturmented we can see examples of what that would look like when your tracing the pipeline, 
stages and commands

![pipeline-trace-with-cmd-wrappers](./images/pipeline-trace-with-cmd-wrappers.png)

or even if just tracing the pipeline and stages

![pipeline-trace](./images/pipeline-trace.png)